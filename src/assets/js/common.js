$(function() {

  /* ------------------
  TOP PAGE AND COMMON
  ---------------------*/

  // set window width in init and resize
  var wWindow = $(window).outerWidth();
  $(window).resize(function () {
    wWindow = $(window).outerWidth();
    $('.js-main-slider')[0].slick.refresh()
  });


  // Open menu
  $('.js-btn-menu').click(function() {
    if (wWindow <= 950) {
      $('body').toggleClass('fixed');
    }
    $(this).toggleClass('active');
    $('.js-menu').toggleClass('active');
  });

  // Close menu
  $('.js-menu-link, .js-order-link').click( function() {
    $('body').removeClass('fixed');
    $('.js-btn-menu , .js-menu').removeClass('active');
  });

  // Animation on scroll
  $(window).scroll(function() {
    // Scroll slide up out
    var scroll = $(window).scrollTop();
    var windowHeight = $(window).height();

    $('.fadeup').each(function() {
      let elemPos = $(this).offset().top;
      if (scroll > elemPos - windowHeight + 80) {
        $(this).addClass('fadeup-in');
      }
    });
  });


  // Scroll to section
  $('a[href^="#"]').not("a[class*='carousel-control-']").click(function() {
    var href= $(this).attr("href");
    var hash = href == "#" || href == "" ? 'html' : href;
    var position = $(hash).offset().top - 100;
    $('body,html').stop().animate({scrollTop:position}, 700);
    return false;
  });

  // Main Slider
  $('.js-main-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    swipeToSlide: false,
    fade: true,
    pauseOnFocus: false,
    pauseOnHover: false,
    arrows: false,
    dots: true,
    customPaging: function(slider, i) {
      return '<span class="dot"></span>';
    }
  });


   // Show or Hide The Backtop Button
  $(window).scroll(function() {
    if ($(this).scrollTop() > 200) {
      $('.js-backtop').fadeIn(200);
    } else {
      $('.js-backtop').fadeOut(200);
    }
  });


  // Animate the scroll to top
  $('.js-backtop').click(function(e) {
    $('html,body').animate({ scrollTop: 0 }, 300);
  });


});

